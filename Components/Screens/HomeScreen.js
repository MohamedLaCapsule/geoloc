import React from 'react';
import {
 View,
 ImageBackground
} from 'react-native';
import {Avatar, Text, Button, Divider} from 'react-native-elements'


export default class HomeScreen extends React.Component {


 render() {
   return (
     <ImageBackground style={{flex:1}} source={require("../../assets/Images/ImgHomeFirstTime.png")}>
     {/*  ImageBackground is provided by react-native. */}

     <View style={{flex:1, justifyContent:'center', alignItems:'center'}}>

           <Text h1 style={{color: "#FFFFFF"}}>Just'Go</Text>

           <Button
             title="Carte"
             style={{width:100}}
             backgroundColor='#3498db'
             onPress={ () => this.props.navigation.navigate('Carte')}
             >
           </Button>
           <Divider style={{height:20}}/>
           <Button
             title="Firebase"
             style={{width:100}}
             backgroundColor='#3498db'
             onPress={ () => this.props.navigation.navigate('Firebase')}
             >
           </Button>

     </View>

     </ImageBackground>    );
 }
}
