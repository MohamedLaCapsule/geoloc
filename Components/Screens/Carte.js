import React from 'react';
import { View, Text, StyleSheet , Button, Platform} from "react-native";


import MapView, {Marker} from 'react-native-maps';
import * as Location from 'expo-location'
import * as Permissions from 'expo-permissions'
import firebase from '../../firebase/firebase';



export default class Carte extends React.Component {

  constructor(){
   super();
   this.state = { currentPosition: {latitude:0, longitude:0}}
  }


  componentWillMount() {
     if (Platform.OS === 'android' && !Constants.isDevice) {
       this.setState({
         errorMessage: 'Oops, this will not work on Sketch in an Android emulator. Try it on your device!',
       });
     } else {
       this._getLocationAsync();
     }
   }

   componentDidMount() {
     this._getLocationAsync();
   }

    _getLocationAsync = async () => {
      let { status } = await Permissions.askAsync(Permissions.LOCATION);
      if (status !== 'granted') {
        this.setState({
          errorMessage: 'Permission to access location was denied',
        });
      }

      let location = await Location.getCurrentPositionAsync({});
      this.setState({ location });
      var currentPosition = {latitude: location.coords.latitude, longitude: location.coords.longitude};
          this.setState({currentPosition});
          console.log('ici la currentposition', currentPosition);
    };


    sendActivityRequest = () => {

      console.log('fonction, request');
      var ctx = this;

      firebase.auth().onAuthStateChanged((user) => {
        if (user) {
          // FETCH OTHER PROFILE //
      var OtherDocRef = database.collection("Post").doc(ctx.props.postId).collection('Activity_Requests_Reiceved');
          //                    //
          // SEND FRIEND REQUEST //
        OtherDocRef.add({
          latitude: ctx.state.currentPosition.latitude,
          longitude: ctx.state.currentPosition.longitude,
        })
        .then(function() {

          // FETCH OTHER PROFILE //
      var myDocRef = database.collection("Users").doc(ctx.state.myUser.Id).collection('Post');
          //                    //
          // SEND FRIEND REQUEST //
        myDocRef.add({
          latitude: ctx.state.currentPosition.latitude,
          longitude: ctx.state.currentPosition.longitude,
        })
      }).then(function() {
          console.log("Document successfully written!");
        })
        .catch(function(error) {
          console.error("Error writing document: ", error);
        })
        //                          //
      }
      })
    }

  render() {
    return (

        <View style={{flex : 1}}>

          <MapView style={{flex : 1}}
          region={{ latitude: this.state.currentPosition.latitude, longitude: this.state.currentPosition.longitude, latitudeDelta: 0.0922, longitudeDelta: 0.0421 }}
         onRegionChange={this._handleMapRegionChange}
          >

          <Marker key={"currentPos"}
          pinColor="red"
          title="Ma Position"
          description="Récente"
          coordinate={{latitude: this.state.currentPosition.latitude, longitude: this.state.currentPosition.longitude}}
          />

         </MapView>

         <Text style={{textAlign:'center'}}>
             Lat: {this.state.currentPosition.latitude}
              <Text> Lont: {this.state.currentPosition.longitude} </Text>
           </Text>

         <Button title="Envoi vers Firebase" onPress={ () => this.sendActivityRequest() }/>

        </View>
    );
  }

}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  }
});



// import React from 'react';
// import { Platform, View, Text, StyleSheet } from 'react-native';
// import {Button, FormLabel, FormInput} from 'react-native-elements';
// import Constants from 'expo-constants';
//
// import MapView, {Marker} from 'react-native-maps';
// import * as Location from 'expo-location';
// import * as Permissions from 'expo-permissions'
//
//
// export default class Carte extends React.Component {
//   constructor(){
//    super();
//    this.state = {
//      location: null,
//      errorMessage:null,
//      currentPosition:
//      {latitude:0, longitude:0},
//      logPosition:[],
//      displayHistorique: true}
//   }
//
//
//   componentWillMount() {
//  if (Platform.OS === 'android' && !Constants.isDevice) {
//    this.setState({
//      errorMessage: 'Oops, this will not work on Sketch in an Android emulator. Try it on your device!',
//    });
//  } else {
//    this._getLocationAsync();
//  }
// }
//
// _getLocationAsync = async () => {
//  let { status } = await Permissions.askAsync(Permissions.LOCATION);
//  if (status !== 'granted') {
//    this.setState({
//      errorMessage: 'Permission to access location was denied',
//    });
//  }
//
//  let location = await Location.getCurrentPositionAsync({});
//  this.setState({ location });
// };
//
//
//   render() {
//
//     let text = 'Waiting..';
// if (this.state.errorMessage) {
//   text = this.state.errorMessage;
// } else if (this.state.location) {
//   text = JSON.stringify(this.state.location);
// }
//
//     return (
//       <View style={styles.container}>
//     <Text style={styles.paragraph}>{text}</Text>
//   </View>
//     )
//   }
// }
//
//
// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     alignItems: 'center',
//     justifyContent: 'center',
//     paddingTop: Constants.statusBarHeight,
//     backgroundColor: '#ecf0f1',
//   },
//   paragraph: {
//     margin: 24,
//     fontSize: 18,
//     textAlign: 'center',
//   },
// });
