import firebase from 'firebase/app';
  import 'firebase/auth';
  import 'firebase/database'
  import 'firebase/firestore'
  import 'firebase/storage'


   export const config = {
     apiKey: "AIzaSyCuQ-B5h76ui4flBst5eePK22xRLlZxr98",
     authDomain: "geoloc-27778.firebaseapp.com",
     databaseURL: "https://geoloc-27778.firebaseio.com",
     projectId: "geoloc-27778",
     storageBucket: "geoloc-27778.appspot.com",
     messagingSenderId: "1065227041400",
     appId: "1:1065227041400:web:af3f3ee0ae7058e0"
   };


   if (!firebase.apps.length) {
     firebase.initializeApp(config);
   }

   const auth = firebase.auth();
   export const storage = firebase.storage();
   export const provider = new firebase.auth.FacebookAuthProvider();
   export const database = firebase.firestore();


   export default firebase
